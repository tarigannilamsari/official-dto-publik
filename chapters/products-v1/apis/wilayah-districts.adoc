// Note: For source code, Slim and Erlang language just for syntax highlighting only, not actual language
:reset-sectlevel: 2+
include::{dir-directive}/section-reset.adoc[]

include::{dir-snippet}/admonition/important-variable-asterisk.adoc[]


==== {level1}.{counter:level2}. Request

include::{dir-snippet}/apidoc/req-target-url.adoc[]

include::{dir-snippet}/apidoc/req-target-method.adoc[]

// Request parameter orders are: Header, Query String, Body

===== Header

[cols="2,^1,5a",separator="|"]
|===
|Nama Parameter |Tipe Data |Keterangan

include::{dir-snippet}/apidoc/req-param-integration.adoc[]

|===

==== Query String

[cols="2,^1,5a",separator="|"]
|===
|Nama Parameter |Tipe Data |Keterangan

|`*city_codes`
|`number`
|Isi dengan Kode kabupaten/kota berdasarkan Kode Kemendagri. (dapat di-_input_ multi dengan menyertakan koma)

Contoh: `{qs-cities}`.

|`codes`
|`number`
|Isi dengan Kode kecamatan berdasarkan Kode Kemendagri. (dapat di-_input_ multi dengan menyertakan koma)

Contoh: `{qs-codes}`.

|===


==== {level1}.{counter:level2}. Response

Hasil _response_, dengan HTTP _Status Code_ berpola `2xx` atau `4xx`, yang dikembalikan dari server mempunyai parameter `{http-header-content}` dengan nilai `{mime-json}` di salah satu parameter _header_-nya.

===== 2xx _Success_

*Contoh Data*

include::{dir-snippet}/admonition/warning-dummy-value.adoc[]

:res-file-name: res-2xx.jsonc
[source,json,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----

===== 4xx _Client Error_

Sistem akan mengembalikan pesan _error_ bila _client_ belum melakukan autentikasi, tidak memiliki akses, menggunakan HTTP _method_ yang tidak tepat, atau mengirimkan data dengan format atau ketentuan yang tidak sesuai.

*Contoh Data*

:res-file-name: res-4xx.jsonc
[source,json,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----

===== 5xx _Server Error_ (`{http-header-content}: {mime-text}`)
Sistem akan mengembalikan pesan _error_ bila terjadi kesalahan pada sisi server saat memproses data yang telah dikirimkan.

*Contoh Data*

:res-file-name: res-5xx.txt
[source,text,subs=attributes+]
----
include::{dir-snippet}/apidoc/file-example.adoc[]
----


==== {level1}.{counter:level2}. Contoh Pengunaan/Kode

include::{dir-snippet}/admonition/warning-dummy-value.adoc[]

// curl
include::{dir-snippet}/apidoc/example-curl.adoc[]

// postman
include::{dir-snippet}/apidoc/example-postman-start.adoc[]

include::{dir-snippet}/apidoc/example-postman-part-auth.adoc[]

include::{dir-snippet}/apidoc/example-postman-part-headers.adoc[]

include::{dir-snippet}/apidoc/example-postman-part-params-query.adoc[]

include::{dir-snippet}/apidoc/example-postman-end.adoc[]
