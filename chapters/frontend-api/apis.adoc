[#fe-api-auth-signin]
=== {counter:level1}. Autentikasi - Masuk Sesi

:api-verb: POST
:api-uri: /auth/sign/in
:api-params:
:file-context: auth-signin
include::apis/{file-context}.adoc[]
