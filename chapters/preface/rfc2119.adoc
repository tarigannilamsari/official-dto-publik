:reset-sectlevel: 2+
include::{dir-directive}/section-reset.adoc[]

Dalam banyak dokumen terkait standar, beberapa kata digunakan untuk menandakan persyaratan dalam spesifikasi. Kata-kata ini sering dikapitalisasi dan/atau ditebalkan. Pada bagian ini akan didefinisikan kata-kata ini seperti yang dilakukan pada banyak dokumen RFC (_Request for Comment_), dengan beracuan pada standar di {rfc-guide}[https://www.ietf.org/rfc/rfc2119.txt] beberapa kata-kata tersebut dialihbahasakan ke bahasa Indonesia.

- {rfc-must}, berarti definisi tersebut merupakan persyaratan mutlak dari spesifikasi.

- {rfc-must-not}, berarti definisi tersebut merupakan larangan mutlak terhadap spesifikasi.

- {rfc-should}, berarti bahwa ada alasan yang sah dan kuat dalam keadaan tertentu untuk mengesampingkan hal tertentu lainnya, tetapi implikasi dari perilaku tersebut harus dipahami dan dipertimbangkan dengan cermat sebelum memilih alur yang berbeda.

- {rfc-should-not}, berarti bahwa ada alasan yang sah dan kuat dalam keadaan tertentu ketika perilaku tertentu dapat diterima atau bahkan berguna, tetapi implikasi dari perilaku harus dipahami dan kasusnya dipertimbangkan dengan cermat sebelum menerapkan perilaku apa pun yang dijelaskan dengan label ini.

- {rfc-recommended}, sama definisinya dengan {rfc-should}.

- {rfc-not-recommended}, sama definisinya dengan {rfc-should-not}.

- {rfc-may}, berarti bahwa suatu spesifikasi benar-benar opsional. Satu vendor dapat memilih untuk memasukkan spesifikasi tersebut karena kondisi tertentu membutuhkannya atau karena vendor merasa bahwa itu meningkatkan performa tertentu sementara vendor lain mungkin menghilangkan spesifikasi yang sama. Implementasi yang tidak menyertakan opsi tertentu {rfc-required} siap untuk beroperasi dengan implementasi lain yang menyertakan opsi, meskipun mungkin dengan fungsionalitas yang berkurang. Dalam hal yang sama, implementasi yang menyertakan opsi tertentu {rfc-required} siap untuk beroperasi dengan implementasi lain yang tidak menyertakan opsi (kecuali, tentu saja, untuk fitur yang disediakan opsi.)

- {rfc-optional}, sama definisinya dengan {rfc-may}.

Penggunaan kata-kata tadi biasanya untuk panduan dari suatu spesifikasi agar berjalan sesuai yang diharapkan. Bisa juga terkait pertimbangan keamanan, dimana kata-kata tersebut sering digunakan untuk menentukan perilaku dengan implikasi keamanan. Efek pada keamanan karena tidak menerapkan {rfc-must} atau {rfc-should}, atau melakukan sesuatu yang menurut spesifikasi {rfc-must-not} atau {rfc-should-not} dilakukan mungkin sangat bijak.
