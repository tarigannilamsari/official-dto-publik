:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

// Reference format:
// * [[[reference-label]]]
// Author. (optional)
// "Book/Article Title".
// Publisher. (optional)
// Site URL. (optional)

* [[[restfulapi.net]]]
"ReST API Tutorial".
https://restfulapi.net.

* [[[wikipedia-rest]]]
"Representational State Transfer".
https://en.wikipedia.org/wiki/Representational_state_transfer.

* [[[wikipedia-http-method]]]
"Hypertext Transfer Protocol, Request_methods".
https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods

* [[[wikipedia-http-status-code]]]
"List of HTTP status codes".
https://en.wikipedia.org/wiki/List_of_HTTP_status_codes

* [[[wikipedia-iso8601]]]
"ISO 8601".
https://en.wikipedia.org/wiki/ISO_8601

* [[[wikipedia-API]]]
"Application Programming Interface".
https://en.wikipedia.org/wiki/API
