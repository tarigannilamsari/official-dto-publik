:reset-sectlevel: 2+
include::{dir-directive}/section-reset.adoc[]

Kode aplikasi {fe-app-name} menggunakan beberapa teknologi, _library_, atau _package_ sebagai berikut:

- https://nodejs.org/en/[{tech-node}] sebagai _platform_ utama aplikasi ini akan berjalan.

Untuk melihat _library_ atau _package_ lainnya yang digunakan silakan lihat lebih detail pada berkas `package.json`.

<<<
