[#api-wilayahv2-province]
=== {counter:level1}. Master Wilayah - Provinsi

:api-verb: GET
:api-uri: /v2/provinces?current_page
:api-params:
:qs-codes: 11
:path-codes: pass:q,a[codes={qs-codes}]
//:api-request-mime: {mime-urlencoded}
:file-context: wilayah-province
:res-api-context: {api-context}/{file-context}
include::apis/{file-context}.adoc[]

<<<

[#api-wilayahv2-cities]
=== {counter:level1}. Master Wilayah - Kota/Kabupaten

:api-verb: GET
:api-uri: /v2/cities?current_page
:api-params:
:qs-province: 11
:path-province: pass:q,a[province={qs-province}]
:qs-codes: 1103,1104, 1210
:path-codes: pass:q,a[codes={qs-codes}]
:path-cities: pass:q,a[{path-province}&{path-codes}]
//:api-request-mime: {mime-urlencoded}
:file-context: wilayah-cities
:res-api-context: {api-context}/{file-context}
include::apis/{file-context}.adoc[]

<<<

[#api-wilayahv2-district]
=== {counter:level1}. Master Wilayah - Kecamatan

:api-verb: GET
:api-uri: /v2/districts?current_page
:api-params:
:qs-cities: 1103, 1104
:path-cities: pass:q,a[cities={qs-cities}]
:qs-codes: 110302, 110301
:path-codes: pass:q,a[codes={qs-codes}]
:path-districts: pass:q,a[{path-cities}&{path-codes}]
//:api-request-mime: {mime-urlencoded}
:file-context: wilayah-districts
:res-api-context: {api-context}/{file-context}
include::apis/{file-context}.adoc[]

<<<

[#api-wilayahv2-subdistrict]
=== {counter:level1}. Master Wilayah - Kelurahan/Desa

:api-verb: GET
:api-uri: /v2/sub-districts?current_page
:api-params:
:qs-districts: 110301, 110302
:path-districts: pass:q,a[districts={qs-districts}]
:qs-codes: 1103012002, 1103012004 ,1103022004
:path-codes: pass:q,a[codes={qs-codes}]
:path-subdistricts: pass:q,a[{path-districts}&{path-codes}]
//:api-request-mime: {mime-urlencoded}
:file-context: wilayah-sub-districts
:res-api-context: {api-context}/{file-context}
include::apis/{file-context}.adoc[]

<<<
