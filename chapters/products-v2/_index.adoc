:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

:api-context: products/v2
:api-endpoint: {products-api-url-wilayah}

include::intro.adoc[]
include::apis.adoc[]
