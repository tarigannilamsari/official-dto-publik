[#auth-endpoint]
=== {counter:level1}. Endpoint

Pada bagian ini akan dijelaskan spesifikasi untuk {auth-endpoint-name}, yang mempunyai _endpoint_ berdasarkan jenis lingkungan pengembangannya (_development environment_) yaitu:

* _development_ https://api-satusehat-dev.dto.kemkes.go.id/oauth2/v1
* _staging_ https://api-satusehat-stg.dto.kemkes.go.id/oauth2/v1
* _production_ https://api-satusehat.kemkes.go.id/oauth2/v1

include::{dir-snippet}/admonition/note-endpoint-prod-only.adoc[]
