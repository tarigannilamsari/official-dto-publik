:reset-sectlevel: all
include::{dir-directive}/section-reset.adoc[]

:app-context: backend

Pada bagian ini akan dijelaskan mengenai struktur kode dan beberapa konfigurasi pada aplikasi {be-app-name}.

include::{dir-snippet}/admonition/tip-text-blue.adoc[]


[#be-app-tech]
=== {counter:level1}. Teknologi yang Digunakan

include::technology.adoc[]


[#be-app-repo]
=== {counter:level1}. Repositori Kode

include::repository.adoc[]


<<<
