ifdef::confidentialy-level[]
ifeval::["{confidentialy-level}" != "none"]

:subject-dto: pass:q[*Digital Transformation Office (DTO) – Kementerian Kesehatan Republik Indonesia*]

== Kerahasiaan Informasi

include::{confidentialy-level}.adoc[]

endif::[]
endif::confidentialy-level[]
