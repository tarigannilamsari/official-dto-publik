:revremark: TERBATAS
:disclaimer: pass:q,a[Materi ini bersifat *{revremark}*. DILARANG menggunakan, menduplikasi, mempublikasi, dan/atau mendistribusikan  dalam bentuk apapun, sebagian/keseluruhan informasi di dalam dokumen ini, secara elektronik maupun mekanik tanpa izin tertulis dari {subject-dto}.]

Informasi yang terkandung dalam dokumen ini bersifat:

[.text-center]
[.confidentiality-level]#{revremark}#

Yang berarti bahwa dokumen dan/atau sebagian/keseluruhan informasi di dalam dokumen ini, terbatas hanya untuk pegawai institusi kesehatan, pengawas internal dan/atau eksternal,  serta penegak hukum, sehingga *DILARANG* menggunakan, menduplikasi, mempublikasi, dan/atau mendistribusikan kembali dokumen ini dan/atau informasi di dalamnya dalam bentuk apapun, secara elektronik maupun mekanik tanpa izin tertulis dari {subject-dto}.

{subject-dto} tidak membuat pernyataan dan tidak memberikan jaminan dalam bentuk apapun sehubungan dengan informasi di dalam dokumen ini, termasuk tetapi tidak terbatas pada keakuratan atau kelengkapan informasi, fakta dan/atau pendapat yang terkandung di dalam dokumen ini.

{subject-dto}, para direktur, para karyawan, dan unit-unit di bawahnya tidak dapat dimintai pertanggungjawaban atas penggunaan dan kepercayaan atas opini, perkiraan, dan temuan dalam dokumen ini.
