require "asciidoctor-pdf"
require_relative 'helper/helper.rb'

class BuildPDF
  def self.clear_screen
    Helper.cli_clear
    puts "[ Build PDF using AsciiDoctor PDF #{Asciidoctor::PDF::VERSION} ]\n\n"
  end

  def self.read_document(file_name)
    return Helper::AsciidoctorDocument.new('book.adoc')
  end

  def self.from_git(main_branch, output_filename, input_filename = 'book.adoc', working_directory = Dir.pwd)
    is_prod = Helper::Git.is_current_branch_clean(main_branch)
    cwd = working_directory.squeeze('/')
    output_path = "#{cwd}/output/"

    mode = is_prod ? 'PRODUCTION' : 'DEVELOPMENT'
    output = Helper.pdf_output_versioned(is_prod, output_path, output_filename)

    command = self.command_create
      # .add_library("#{cwd}/scripts/converter/?.rb")
      .add_attribute('localdate', Helper::Git.date)
      .add_flag('out-file', output)
      .create_arguments(input_filename)
      .join(' ')

    puts "Building latest PDF in #{mode} mode using this Git commit:\n\n"
    puts Helper::Git.log
    puts "\n"

    `#{command}`

    if File.file?(output)
      puts "\nOutput file: #{output}\n\n"
    else
      puts "\nBuild failed!\n\n"
    end
  end

  def self.from_no_git(output_filename, input_filename = 'book.adoc', working_directory = Dir.pwd)
    cwd = working_directory.squeeze('/')
    output_path = "#{cwd}/output/"

    output = Helper.pdf_output(output_path, output_filename)

    command = self.command_create
      # .add_library("#{cwd}/scripts/converter/?.rb")
      .add_flag('out-file', output)
      .create_arguments(input_filename)
      .join(' ')

    puts "This project is not Git versioned\nBuilding latest PDF...\n\n"

    `#{command}`

    if File.file?(output)
      puts "\nOutput file: #{output}\n\n"
    else
      puts "\nBuild failed!\n\n"
    end
  end

  private

  def self.command_create
    command = Helper::AsciidoctorPdfCommand.new(Helper.is_os_windows)

    # Required libraries
    command.add_library('asciidoctor-diagram')

    # Required attributes
    command.add_attribute('pdf-theme', 'book')
    command.add_attribute('pdf-themesdir', 'book/themes')
    command.add_attribute('source-highlighter', 'rouge')
    command.add_attribute('allow-uri-read')
    command.add_attribute('compress')

    # Required flags
    command.add_flag('trace')
    command.add_flag('timings')

    return command
  end
end
