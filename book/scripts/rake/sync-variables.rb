require_relative 'helper/helper.rb'

class SyncVscCodeSnippet
  def self.clear_screen
    Helper.cli_clear
    puts "[ Synchronize Variables ]\n\n"
  end

  def self.sync_variables
    snippet_file = Dir.pwd + '/.vscode/references.code-snippets'
    variable_files = Dir["#{Dir.pwd}/book/variables/*.adoc"]

    vsc_snippet = Helper::VscCodeSnippet.new(snippet_file, variable_files)
    vsc_snippet.sync_variables
  end
end
