require 'uri'

module Helper
  class ReadmeFile
    STATUS_NOT_FOUND = -1
    STATUS_CHANGED = 1
    STATUS_UNCHANGED = 2

    def initialize(file_name)
      if !File.file?(file_name)
        raise "File '#{file_name}' not found!"
      end

      @file_name = file_name

      file = File.open(file_name)
      @lines = file.readlines
      file.close
    end

    def sync_title(current_title)
      old_title = @lines[0].strip
      new_title = "# #{current_title.strip}"
      status = STATUS_NOT_FOUND

      if !old_title.eql?(new_title)
        @lines[0] = new_title
        status = STATUS_CHANGED
      else
        status = STATUS_UNCHANGED
      end

      return [old_title, new_title, status]
    end

    def sync_git_remote(current_remote)
      first_string = "`git clone https://<git-user>:<git-access-token>@"
      last_string = ".git <nama-project>`"
      line_index = -1

      uri = URI.parse(current_remote)
      source_string = [
          "`git clone #{uri.scheme}://<git-user>:<git-access-token>@",
          "#{uri.host}#{uri.path}",
          ' <nama-project>`',
        ].join('')
      target_string = nil
      status = STATUS_NOT_FOUND

      for i in 0..@lines.length
        if @lines[i].index(first_string)
          line_index = i
          break
        end
      end

      if line_index > 0
        found_string = @lines[line_index]
        start_index = found_string.index(first_string)
        end_index = found_string.index(last_string) + last_string.length - 1
        target_string = found_string[start_index..end_index]

        if (!target_string.eql?(source_string))
          @lines[line_index] = found_string.gsub(target_string, source_string)
          status = STATUS_CHANGED
        else
          status = STATUS_UNCHANGED
        end
      end

      return [target_string, source_string, status]
    end

    def update
      File.write(@file_name, @lines.join("\n"))
    end
  end
end
