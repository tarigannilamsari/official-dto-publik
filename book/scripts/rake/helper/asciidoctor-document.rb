module Helper
  class AsciidoctorDocument
    def initialize(file_name)
      if !File.file?(file_name)
        raise "File '#{file_name}' not found!"
      end

      file = File.open(file_name)
      full_title = file.readline
      file.close

      title, subtitle = full_title.gsub('pass:q[<br>]', '').gsub('=', '').squeeze(' ').split(':')

      @title = title.strip
      @subtitle = subtitle.strip
    end

    def get_title
      return @title
    end

    def get_subtitle
      return @subtitle
    end
  end
end
