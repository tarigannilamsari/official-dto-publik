require 'json'

module Helper
  class VscCodeSnippet
    START_MARK = '${1|{'
    END_MARK = '}|}'
    DELIMITER_MARK = '},{'

    def initialize(snippet_file, variable_files)
      if !File.file?(snippet_file)
        raise "File '#{snippet_file}' not found!"
      end

      if !variable_files.is_a?(Array)
        raise "Parameter 'variable_files' must array of *.adoc file contain variables definition!"
      end

      variable_files.each do |file|
        if !File.file?(file)
          raise "File '#{file}' not found!"
        end
      end

      @snippet_file = snippet_file
      @snippet_data = {}
      @snippet_variables = []
      @snippet_variables_diff = []
      @variable_files = variable_files
      @variables = []
      @variables_diff = []
    end

    def sync_variables
      read_snippets
      read_variables

      puts @snippet_data['Variables']['body']

      new_body = [
        START_MARK,
        @variables.join(DELIMITER_MARK),
        END_MARK,
      ].join('')

      puts new_body

      @snippet_data['Variables']['body'] = new_body
    end

    private

    def read_variables
      @variable_files.each { |file| parse_variable_file(file) }

      unique_variables = @variables.uniq
      is_duplicate, diff = check_duplication(@variables, unique_variables)

      if is_duplicate
        @variables = unique_variables
        @variables_diff = diff
      end
    end

    def parse_variable_file(file)
      File.readlines(file).map { |line| line.strip } .each do |line|
        variables = line[1..(line.index(': ') - 1)]
        @variables.push(variables)
      end
    end

    def read_snippets
      json_string = File.read(@snippet_file)
      @snippet_data = JSON.parse(json_string)
      @snippet_variables = parse_snippet_body(@snippet_data['Variables']['body'])

      unique_variables = @snippet_variables.uniq
      is_duplicate, diff = check_duplication(@snippet_variables, unique_variables)

      if is_duplicate
        @snippet_variables = unique_variables
        @snippet_variables_diff = diff
      end
    end

    def parse_snippet_body(body)
      if body.start_with?(START_MARK)
        variables = body[(START_MARK.length)...(body.length - END_MARK.length)]

        return variables.split(DELIMITER_MARK)
      end

      return []
    end

    def check_duplication(old_array, new_array)
      diff_array = old_array - new_array

      return [diff_array.length > 0, diff_array]
    end
  end
end
