// Must be changed with latest values
:revnumber: DRAFT
:revdate: 31 September 2022

== Riwayat Perubahan

[cols="1,2,3,2"]
|===
|Versi |Tanggal Pembaruan |Deskripsi Perubahan |Versi Aplikasi

| 1.0
| 20 Februari 2024
| *Penyusunan* TASK Dokumentasi Teknis SATUSEHAT: Master Wilayah ReST API versi Asciidoc pdf
| 
|

|===
