# TITLE, SUBTITLE

## Perangkat Lunak yang Dibutuhkan

- Ruby 2.5+ Development Tools, (Disarankan Ruby 3+).
- Java Runtime.
- Visual Studio Code (untuk _editing_).
- Git LFS.
- SumatraPDF (opsional, tetapi disarankan untuk OS Windows).


## Langkah-langkah Instalasi Perangkat Lunak

1. Install VSCode sesuai sistem operasi yang digunakan.
    - Link download _https://code.visualstudio.com/download/_
2. Install Git LFS.
    - Link download _https://git-lfs.github.com/_
3. Install Ruby sesuai sistem operasi yang digunakan.
    - Link download _https://www.ruby-lang.org/en/downloads/_
4. Install Java Runtime sesuai sistem operasi yang digunakan.
    - Link download _https://www.java.com/en/download/_
5. Setelah instalasi Ruby dan Java Runtime selesai, buka console/terminal/command prompt.
6. Ketik `ruby --version` lalu tekan [Enter], jika tampil versi Ruby, maka instalasi sukses.
7. Ketik `java --version` lalu tekan [Enter], jika tampil versi Java Runtime, maka instalasi sukses.
8. Selanjutnya install Bundler, dengan mengetik `gem install bundler` lalu tekan [Enter].
9. Setelah instalasi selesai, ketik `bundle --version` lalu tekan [Enter], jika muncul versi Bundler, maka instalasi sukses.


## Langkah-langkah untuk Membuat Project

1. Buka console/terminal/command prompt, dan pergi ke direktori yang diinginkan dimana project ini akan di_clone_.
2. __Git clone__ dengan format `git clone https://<git-user>:<git-access-token>@HOST/REPOSITORY.git <nama-project>`.
    - Isi `<git-user>` dengan _username_ saat _login_.
    - Isi `<git-access-token>` dengan _access token_ yang sudah dibuat sebelumnya.
    - Isi `<nama-project>` dengan nama project yang akan dibuat.
    - Jika sudah tekan [Enter], tunggu hingga proses _cloning_ selesai.
3. Ketik `cd <nama-project>` lalu tekan [Enter] untuk masuk ke direktori hasil _clone_ tadi.
4. Ketik `bundle install` lalu tekan [Enter] untuk melakukan proses instalasi dependensi project (AsciiDoctor, dll). Tunggu hingga selesai.
5. Buka editor Visual Studio Code, bisa dengan mengetik `code .` bila mendukung, atau dari context menu `Open with VSCode`.
6. Project siap dikerjakan.


## Langkah-langkah untuk Membuat PDF

### Production

1. Sebelumnya pastikan tidak ada perubahan yang belum di-_commit_, dengan mengetik di console/terminal/command prompt: `git status` lalu tekan [Enter].
2. PDF versi _production_ hanya bisa dibuat di git _branch_ __master__, jadi perlu dilakukan _merge_ terlebih dahulu. Bila tetap ingin membuat PDF versi _production_ maka, sistem akan mengambil dari git _commit_ terakhir dari _branch_ __master__. Silakan lanjut ke langkah berikutnya.
3. Ketik `git checkout master` lalu tekan [Enter]. untuk pindah ke _branch_ __master__.
4. Ketik `bundle exec rake build:pdf` lalu tekan [Enter], untuk membuat berkas PDF dari kode _commit_ terakhir yang berada di _branch_ __master__.
5. Bila sukses, berkas PDF akan berada di `/output/<judul buku>.pdf`. Setiap _build_ PDF terbaru, PDF lama akan ditimpa ulang.

### Development

1. Sebelumnya pastikan tidak ada perubahan yang belum di-_commit_, dengan mengetik di console/terminal/command prompt: `git status` lalu tekan [Enter].
2. PDF versi _development_ bisa dibuat sewaktu-waktu, tentunya di git _branch_ selain __master__.
3. Ketik `bundle exec rake build:pdf` lalu tekan [Enter], untuk membuat berkas PDF dari kode commit terakhir yang berada di _branch_ aktif non __master__.
4. Bila sukses, berkas PDF akan berada di `/output/[WIP] <judul buku>.pdf`. Setiap _build_ PDF terbaru, PDF lama akan ditimpa ulang.


## Perangkat Lunak untuk Pratinjau PDF

Khusus untuk sistem operasi Windows, untuk kebutuhan pratinjau PDF hasil _build_ dari Asciidoctor disarankan memakai SumatraPDF. Ini dikarenakan aplikasi SumatraPDF tidak mengunci berkas PDF yang sedang dibaca, sehingga ketika PDF yang baru tersedia, aplikasi SumatraPDF akan otomatis memuat PDF yang terbaru. Aplikasi SumatraPDF bisa di unduh di https://www.sumatrapdfreader.org/download-free-pdf-viewer.
